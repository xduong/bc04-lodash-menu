import React, { useState } from "react";
import _ from "lodash";
import Modal from "./Modal";
const data = [
  {
    maMonAn: "MA01",
    tenMonAn: "Cơm chiên dương châu",
    gia: 200,
    hinhAnh: "http://casestudy.cyberlearn.vn/img/MA01.jpeg",
    maDanhMuc: "MC",
    tenDanhMuc: "Món chiên",
  },
  {
    maMonAn: "MA02",
    tenMonAn: "Cơm chiên cá mặn",
    gia: 300,
    hinhAnh: "http://casestudy.cyberlearn.vn/img/MA02.jpeg",
    maDanhMuc: "MC",
    tenDanhMuc: "Món chiên",
  },
  {
    maMonAn: "MA03",
    tenMonAn: "Gà nướng muối ớt",
    gia: 500,
    hinhAnh: "http://casestudy.cyberlearn.vn/img/MA03.jpeg",
    maDanhMuc: "MN",
    tenDanhMuc: "Món nướng",
  },
  {
    maMonAn: "MA04",
    tenMonAn: "Gà nướng muối tiêu chanh",
    gia: 600,
    hinhAnh: "http://casestudy.cyberlearn.vn/img/MA04.jpeg",
    maDanhMuc: "MN",
    tenDanhMuc: "Món nướng",
  },
  {
    maMonAn: "MA05",
    tenMonAn: "Trà đào cam sả",
    gia: 50,
    hinhAnh: "http://casestudy.cyberlearn.vn/img/MA05.jpeg",
    maDanhMuc: "GK",
    tenDanhMuc: "Giải khát",
  },
  {
    maMonAn: "MA06",
    tenMonAn: "Bia heniken",
    gia: 50,
    hinhAnh: "http://casestudy.cyberlearn.vn/img/MA06.jpeg",
    maDanhMuc: "GK",
    tenDanhMuc: "Giải khát",
  },
];

export default function Lodash_Menu() {
  const mangMaDanhMuc = _.uniqBy(data, "maDanhMuc");

  const [maDanhMuc, setMaDanhMuc] = useState(mangMaDanhMuc[0].maDanhMuc);

  const [modal, setModal] = useState(false);

  const [monAn, setMonAn] = useState({});

  const mangMonAnDanhMuc = _.filter(
    data,
    (item) => item.maDanhMuc === maDanhMuc
  );

  const handleChangeDanhMuc = (maDanhMuc) => {
    return setMaDanhMuc(maDanhMuc);
  };

  const handlePopUP = (monAn) => {
    setModal(true);
    setMonAn(monAn);
  };

  const renderMenu = () => {
    return mangMaDanhMuc.map((item) => {
      let classNameAcive = [
        "p-2",
        "space-x-3",
        "rounded-md",
        "w-full",
        item.maDanhMuc === maDanhMuc ? "bg-sky-400 text-white" : "",
      ];
      return (
        <li className="bg-gray-100 text-gray-900">
          <button
            className={classNameAcive.join(" ")}
            onClick={() => {
              handleChangeDanhMuc(item.maDanhMuc);
            }}
          >
            <span>{item.tenDanhMuc}</span>
          </button>
        </li>
      );
    });
  };
  const renderMonAn = () => {
    return mangMonAnDanhMuc.map((item) => {
      return (
        <div className="p-4 lg:w-1/3">
          <div className="h-full bg-gray-100 bg-opacity-75 px-8 py-4 rounded-lg overflow-hidden text-center relative">
            <h1 className="title-font sm:text-2xl text-xl font-medium text-gray-900 mb-4">
              {item.tenMonAn}
            </h1>
            <img src={item.hinhAnh} className="rounded-lg mb-4" />

            <div className="text-center mt-2 leading-none flex justify-between w-full ">
              <button
                className="bg-sky-400 text-white p-2 space-x-3 rounded-md hover:bg-sky-700 duration-500"
                onClick={() => {
                  handlePopUP(item);
                }}
              >
                Learn More
              </button>
              <span className="text-2xl">$ {item.gia}</span>
            </div>
          </div>
        </div>
      );
    });
  };
  return (
    <>
      <Modal modal={modal} setModal={setModal} monAn={monAn} />
      <div className="container mx-auto grid grid-cols-6">
        <div className="col-span-1 h-full p-3 space-y-2  bg-gray-50 text-gray-800">
          <div className="flex items-center p-2 space-x-4">
            <img
              src="https://source.unsplash.com/100x100/?portrait"
              alt=""
              className="w-12 h-12 rounded-full bg-gray-500"
            />
            <div>
              <h2 className="text-lg font-semibold">Leroy Jenkins</h2>
              <span className="flex items-center space-x-1">
                <a
                  rel="noopener noreferrer"
                  href="#"
                  className="text-xs hover:underline text-gray-600"
                >
                  View profile
                </a>
              </span>
            </div>
          </div>
          <div className="divide-y divide-gray-300">
            <ul className="pt-2 pb-4 space-y-1 text-sm">{renderMenu()}</ul>
          </div>
        </div>
        <section className="col-span-5 text-gray-600 body-font">
          <div className="container px-5 py-24 mx-auto">
            <div className="flex flex-wrap -m-4">{renderMonAn()}</div>
          </div>
        </section>
      </div>
    </>
  );
}
