import logo from "./logo.svg";
import "./App.css";
import Lodash_Menu from "./Lodash_Menu/Lodash_Menu";

function App() {
  return (
    <div className="App">
      <Lodash_Menu />
    </div>
  );
}

export default App;
